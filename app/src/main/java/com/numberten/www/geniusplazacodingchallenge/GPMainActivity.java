package com.numberten.www.geniusplazacodingchallenge;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.numberten.www.geniusplazacodingchallenge.Recycler.GPListAdapter;
import com.numberten.www.geniusplazacodingchallenge.model.UserResponse;
import com.numberten.www.geniusplazacodingchallenge.network.GPApplication;

import java.util.Date;

public class GPMainActivity extends AppCompatActivity implements Response.Listener<String>, Response.ErrorListener{

    private ProgressBar loading;
    private RecyclerView recyclerView;
    private String tag = "";
    // incrementing the page could be used for pagination
    private int currentPage = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        loading = findViewById(R.id.gpProgressBar);
        recyclerView = findViewById(R.id.gpRecyclerView);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(llm);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(GPMainActivity.this, GPAddUserActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        tag = new Date().toString();
        loading.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.INVISIBLE);
        String url = "https://reqres.in/api/users?page=" + currentPage;
        GPApplication.getInstance().getData(tag, this, url, this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        // kill any pending request
        GPApplication.getInstance().cancelPendingRequests(tag);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(R.string.error_text);
        alertDialog.setMessage(error.getMessage());
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
        loading.setVisibility(View.INVISIBLE);

    }

    @Override
    public void onResponse(String response) {
        if (response != null || !response.isEmpty()) {
            UserResponse userResponse = new Gson().fromJson(response, UserResponse.class);
            loading.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            GPListAdapter gpListAdapter = new GPListAdapter(this);
            gpListAdapter.setUsers(userResponse.getData());
            recyclerView.setAdapter(gpListAdapter);
        }
    }
}
