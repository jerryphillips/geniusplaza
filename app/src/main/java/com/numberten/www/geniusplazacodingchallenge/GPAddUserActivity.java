package com.numberten.www.geniusplazacodingchallenge;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.numberten.www.geniusplazacodingchallenge.model.CreatedUser;
import com.numberten.www.geniusplazacodingchallenge.network.GPApplication;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

public class GPAddUserActivity extends AppCompatActivity implements Response.Listener, Response.ErrorListener{
    private EditText nameEditText;
    private EditText jobEditText;
    private Button addButton;
    private static String NAME = "name";
    private static String JOB = "job";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        nameEditText = findViewById(R.id.nameEditText);
        jobEditText = findViewById(R.id.jobEditText);
        addButton = findViewById(R.id.addButton);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (nameEditText.getText().length() > 0l && jobEditText.getText().length() >0) {
                    String tag = new Date().toString();
                    String url = "https://reqres.in/api/users";
                    JSONObject params = new JSONObject();
                    try {
                        params.put(NAME, nameEditText.getText().toString());
                        params.put(JOB, jobEditText.getText().toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    GPApplication.getInstance().postData(tag, GPAddUserActivity.this, url, params, GPAddUserActivity.this);
                } else {
                    Toast.makeText(GPAddUserActivity.this, R.string.add_text, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(R.string.error_text);
        alertDialog.setMessage(error.getMessage());
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    @Override
    public void onResponse(Object response) {
        if (response != null) {
            CreatedUser createdUser = new Gson().fromJson(String.valueOf(response), CreatedUser.class);
            AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setTitle(R.string.user_created);
            alertDialog.setMessage(createdUser.getName() + "\n" +
                    createdUser.getId() + "\n" +
                    createdUser.getJob() + "\n" +
                    createdUser.getCreatedAt());
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            onBackPressed();
                            dialog.dismiss();
                            finish();
                        }
                    });
            alertDialog.show();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


}
