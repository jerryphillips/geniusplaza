package com.numberten.www.geniusplazacodingchallenge.Recycler;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.numberten.www.geniusplazacodingchallenge.R;

public class UserRowHolder extends RecyclerView.ViewHolder {
    public ImageView avatar;
    public TextView firstName;
    public TextView lastName;

    public UserRowHolder(View itemView) {
        super(itemView);
        avatar = itemView.findViewById(R.id.avatarImageView);
        firstName = itemView.findViewById(R.id.firstNameTextView);
        lastName = itemView.findViewById(R.id.lastNameTextView);
    }
}
