package com.numberten.www.geniusplazacodingchallenge.network;

import android.app.Application;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

public class GPApplication extends Application {
    public static final String TAG = GPApplication.class.getSimpleName();

    private RequestQueue mRequestQueue;

    private static GPApplication mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }


    public static synchronized GPApplication getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public void getData(String tag, Response.Listener listener, String url, Response.ErrorListener errorListener) {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, listener, errorListener);
        stringRequest.setTag(tag);
        addToRequestQueue(stringRequest);
    }

    public void postData(String tag, Response.Listener listener, String url, JSONObject params, Response.ErrorListener errorListener) {
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        postRequest.setTag(tag);
        addToRequestQueue(postRequest);
    }
}
