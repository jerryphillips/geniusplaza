package com.numberten.www.geniusplazacodingchallenge.Recycler;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.numberten.www.geniusplazacodingchallenge.R;
import com.numberten.www.geniusplazacodingchallenge.model.User;

import java.util.ArrayList;
import java.util.List;

public class GPListAdapter extends RecyclerView.Adapter {
    private List<User> users = new ArrayList<>();
    private Context context;

    public GPListAdapter(Context context) {
        this.context = context;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.profile_row, parent, false);
        return new UserRowHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        User user = users.get(position);
        UserRowHolder userRowHolder = (UserRowHolder) holder;
        userRowHolder.firstName.setText(user.getFirstName());
        userRowHolder.lastName.setText(user.getLastName());
        Glide.with(context).load(user.getAvatar()).into(userRowHolder.avatar);
    }

    @Override
    public int getItemCount() {
        return users.size();
    }
}
